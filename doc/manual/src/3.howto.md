# HOWTO

*@open-kappa/node-red-contrib-mydb* uses *mustache* to render queries templates.
The node input is a message JSON object with the values to be substituted, e.g.:

```javascript
msg = {
    "tableName": "test"
}
```

In the node body, write the query tempalte, e.g.:

```sql
select * from {{msg.tableName}};
```

It is worth noting that the syntax with double curly brackets sanitizes the
template parameters. If raw expansion is desired (namely "sql injection"), just use triple curly brackets:

```sql
{{{msg.query}}}
```

In case of insertion of multiple raws, it is possible to prepare the query into
a function node, or to use the following mustache trick.
Suppose to have this input:

```json
[
    {
        "timestamp": "2020-08-31 00:00:00+00",
        "temperature": 20,
        "humidity": 80
    },
    {
        "timestamp": "2020-08-31 00:00:01+00",
        "temperature": 21,
        "humidity": 79
    },
    {
        "timestamp": "2020-08-31 00:00:02+00",
        "temperature": 22,
        "humidity": 78,
        "_last": true
    }
]
```

The template into the node could be:

```sql
INSERT INTO sensors(time, temperature, humidity)
  VALUES
    {{#msg.values}}
    ('{{timestamp}}', {{temperature}}, {{humidity}}){{^_last}},{{/_last}}
    {{/msg.values}}
;
```

Please not the `_last` usage, to get rid of the last comma.
