# FEATURES

* *@open-kappa/node-red-contrib-mydb* supports typescript.
* *@open-kappa/node-red-contrib-mydb* aims to be multi-db.
* *@open-kappa/node-red-contrib-mydb* uses *mustache* to render queries templates.
* *@open-kappa/node-red-contrib-mydb* can be used as a drop-in replacement of
  postgrestor.
* *@open-kappa/node-red-contrib-mydb* supports connections to multiple
  DB's (*postgrestor* cannot).

## Incompatibilities with postgrestor

This package can be used as a drop-in replacement of [postgrestor](
https://github.com/HySoaKa/node-red-contrib-postgrestor), but:

* It has a slightly different behavior in case of multiple SQL commands inside
  the same node: *postgrestor* returns just the last query,
  *@open-kappa/node-red-contrib-mydb* returns a packed object with all
  queryes (which actually can be considered as a bug-fix). This can require
  to adapt the code or query, for example, in case of `set schema` command
  followed by the actual query.

```json
// Single query result:
"payload": {
    "command": ...,
    "rowCount": ...,
    rows: [...],
    ...
}

// Multiple queries result:
"payload": [
    {
        "command": ...,
        "rowCount": ...,
        rows: [...],
        ...
    },
    {
        "command": ...,
        "rowCount": ...,
        rows: [...],
        ...
    },
    ...
]
```
