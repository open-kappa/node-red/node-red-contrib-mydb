/* eslint-disable */

/**
 * @module -- Manual --
 */

export class Manual
{
    private constructor()
    {}

    /**
     * [[include:1.motivations.md]]
     */
    "1. Motivations": void;

    /**
     * [[include:2.features.md]]
     */
    "2. Features": void;

    /**
     * [[include:3.howto.md]]
     */
     "3. Howto": void;

    /**
     * [[include:4.contributing.md]]
     */
    "4. Contributing": void;

    /**
     * [[include:5.copyright.md]]
     */
    "5. Copyright": void;

    /**
     * [[include:6.changelog.md]]
     */
    "6. Changelog": void;
}
