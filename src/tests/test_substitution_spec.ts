import "should";
import {
    MYDB_RUN_MODE,
    type MyDbNodeDef
} from "../node-red-contrib-mydb/src/mydbImpl";
import {
    MyNodeRedTest,
    type TestFlowsItem
} from "@open-kappa/mytest";
import type {
    NodeMessage,
    NodeMessageInFlow
} from "node-red";
import MYDB_BUILDER
    from "../node-red-contrib-mydb/node-red-contrib-mydb";
import MYDB_CONFIG_BUILDER
    from "../node-red-contrib-mydb/node-red-contrib-mydbConfig";

MYDB_RUN_MODE.setTesting(true);

type _Msg = NodeMessage & {
    // eslint-disable-next-line @typescript-eslint/method-signature-style
    boldify?: () => ((text: string, render: (s: string) => string) => string);
    tableName?: string;
    values?: Array<unknown>;
};

type _Payload = {
    command: string;
};

class _ThisTest extends MyNodeRedTest
{
    public constructor()
    {
        super("mydb substitution test", [MYDB_BUILDER, MYDB_CONFIG_BUILDER]);
    }

    protected registerFlows(): void
    {
        const self = this;
        self.registerFlow(
            "Should substitute",
            self._flowSubstitution.bind(self)
        );
        self.registerFlow(
            "Should substitute Mustache",
            self._flowSubstituteMustache.bind(self)
        );
        self.registerFlow(
            "Should substitute Handlebars",
            self._flowSubstituteHandlebars.bind(self)
        );
        self.registerFlow(
            "Should substitute env vars",
            self._flowSubstituteEnvVars.bind(self)
        );
        self.registerFlow(
            "Should NOT substitute env vars",
            self._flowNotSubstituteEnvVars.bind(self)
        );
    }

    protected registerTests(): void
    {
        const self = this;
        self.registerTest(
            "Should substitute",
            self._testSubstitution.bind(self)
        );
        self.registerTest(
            "Should substitute Mustache",
            self._testSubstituteMustache.bind(self)
        );
        self.registerTest(
            "Should substitute Handlebars",
            self._testSubstituteHandlebars.bind(self)
        );
        self.registerTest(
            "Should substitute env vars",
            self._testSubstituteEnvVars.bind(self)
        );
        self.registerTest(
            "Should NOT substitute env vars",
            self._testNotSubstituteEnvVars.bind(self)
        );
    }

    private _flowNotSubstituteEnvVars(): Array<TestFlowsItem>
    {
        const self = this;

        process.env.FOO = "bar";
        const opts: Partial<MyDbNodeDef> = {
            "mydbConfig": "nodeCfg",
            // eslint-disable-next-line no-template-curly-in-string
            "query": "SELECT '${FOO}';",
            "substEnvVars": false
        };

        const cfgNode = self.makeNode("nodeCfg", "nodeCfg1", "mydbConfig");
        let ret = [cfgNode];

        /* eslint-disable function-call-argument-newline */
        ret = ret.concat(
            self.makeNodeWithSourceAndSink(
                "n0", "node0",
                "n1", "node1", "mydb",
                "n2", "node2",
                opts
            )
        );
        /* eslint-enable function-call-argument-newline */

        return ret;
    }

    private _flowSubstituteEnvVars(): Array<TestFlowsItem>
    {
        const self = this;

        process.env.FOO = "bar";
        const opts: Partial<MyDbNodeDef> = {
            "mydbConfig": "nodeCfg",
            // eslint-disable-next-line no-template-curly-in-string
            "query": "SELECT '${FOO}';",
            "substEnvVars": true
        };

        const cfgNode = self.makeNode("nodeCfg", "nodeCfg1", "mydbConfig");
        let ret = [cfgNode];

        /* eslint-disable function-call-argument-newline */
        ret = ret.concat(
            self.makeNodeWithSourceAndSink(
                "n0", "node0",
                "n1", "node1", "mydb",
                "n2", "node2",
                opts
            )
        );
        /* eslint-enable function-call-argument-newline */

        return ret;
    }

    private _flowSubstituteHandlebars(): Array<TestFlowsItem>
    {
        const self = this;

        /* eslint-disable max-len */
        const opts: Partial<MyDbNodeDef> = {
            "mydbConfig": "nodeCfg",
            // This query uses the @last variable which Handlebars supports
            // but Mustache does not
            "query": `INSERT INTO mytable
                        VALUES
                          {{#each msg.values}}
                          ('{{timestamp}}', {{temperature}}, {{humidity}}){{^if @last}},{{/if}}
                          {{/each}}
                      ;`,
            "style": "handlebars"
        };
        /* eslint-enable max-len */

        const cfgNode = self.makeNode("nodeCfg", "nodeCfg1", "mydbConfig");
        let ret = [cfgNode];

        /* eslint-disable function-call-argument-newline */
        ret = ret.concat(
            self.makeNodeWithSourceAndSink(
                "n0", "node0",
                "n1", "node1", "mydb",
                "n2", "node2",
                opts
            )
        );
        /* eslint-enable function-call-argument-newline */

        return ret;
    }

    private _flowSubstituteMustache(): Array<TestFlowsItem>
    {
        const self = this;

        /* eslint-disable max-len */
        const opts: Partial<MyDbNodeDef> = {
            "mydbConfig": "nodeCfg",
            // This query contains a lambda, which Mustache supports but
            // Handlebars does not:
            "query": "SELECT '{{#msg.boldify}}Important table: {{msg.tableName}}{{/msg.boldify}}';",
            "style": "mustache"
        };
        /* eslint-enable max-len */

        const cfgNode = self.makeNode("nodeCfg", "nodeCfg1", "mydbConfig");
        let ret = [cfgNode];

        /* eslint-disable function-call-argument-newline */
        ret = ret.concat(
            self.makeNodeWithSourceAndSink(
                "n0", "node0",
                "n1", "node1", "mydb",
                "n2", "node2",
                opts
            )
        );
        /* eslint-enable function-call-argument-newline */

        return ret;
    }

    private _flowSubstitution(): Array<TestFlowsItem>
    {
        const self = this;

        const opts: Partial<MyDbNodeDef> = {
            "mydbConfig": "nodeCfg",
            "query": "SELECT * FROM {{msg.tableName}};"
        };

        const cfgNode = self.makeNode("nodeCfg", "nodeCfg1", "mydbConfig");
        let ret = [cfgNode];

        /* eslint-disable function-call-argument-newline */
        ret = ret.concat(
            self.makeNodeWithSourceAndSink(
                "n0", "node0",
                "n1", "node1", "mydb",
                "n2", "node2",
                opts
            )
        );
        /* eslint-enable function-call-argument-newline */

        return ret;
    }

    private _testNotSubstituteEnvVars(done: (err?: Error) => void): void
    {
        const self = this;
        const n1 = self.getNode("n1");
        const n2 = self.getNode("n2");
        function checkResult(msg: NodeMessageInFlow): void
        {
            try
            {
                const payload = msg.payload as _Payload;
                const query = payload.command;
                // eslint-disable-next-line no-template-curly-in-string
                query.should.equal("SELECT '${FOO}';");
                done();
            }
            catch (error)
            {
                done(error as Error);
            }
        }
        n2.on("input", checkResult);
        const msg: _Msg = {};
        n1.receive(msg);
    }

    private _testSubstituteEnvVars(done: (err?: Error) => void): void
    {
        const self = this;
        const n1 = self.getNode("n1");
        const n2 = self.getNode("n2");
        function checkResult(msg: NodeMessageInFlow): void
        {
            try
            {
                const payload = msg.payload as _Payload;
                const query = payload.command;
                query.should.equal("SELECT 'bar';");
                done();
            }
            catch (error)
            {
                done(error as Error);
            }
        }
        n2.on("input", checkResult);
        const msg: _Msg = {
        };
        n1.receive(msg);
    }

    private _testSubstituteHandlebars(done: (err?: Error) => void): void
    {
        const self = this;
        const n1 = self.getNode("n1");
        const n2 = self.getNode("n2");
        function checkResult(msg: NodeMessageInFlow): void
        {
            try
            {
                const payload = msg.payload as _Payload;
                const query = payload.command;
                query.should.equal(
                    `INSERT INTO mytable
                        VALUES
                          ('2020-08-31 00:00:00+00', 20, 80),
                          ('2020-08-31 00:00:01+00', 21, 79),
                          ('2020-08-31 00:00:02+00', 22, 78)
                      ;`
                );
                done();
            }
            catch (error)
            {
                done(error as Error);
            }
        }
        n2.on("input", checkResult);
        const msg: _Msg = {
            "values": [
                {
                    "humidity": 80,
                    "temperature": 20,
                    "timestamp": "2020-08-31 00:00:00+00"
                },
                {
                    "humidity": 79,
                    "temperature": 21,
                    "timestamp": "2020-08-31 00:00:01+00"
                },
                {
                    "humidity": 78,
                    "temperature": 22,
                    "timestamp": "2020-08-31 00:00:02+00"
                }
            ]
        };
        n1.receive(msg);
    }

    private _testSubstituteMustache(done: (err?: Error) => void): void
    {
        const self = this;
        const n1 = self.getNode("n1");
        const n2 = self.getNode("n2");
        function checkResult(msg: NodeMessageInFlow): void
        {
            try
            {
                const payload = msg.payload as _Payload;
                const query = payload.command;
                query.should.equal("SELECT '<b>Important table: foobar</b>';");
                done();
            }
            catch (error)
            {
                done(error as Error);
            }
        }

        function boldifyImpl(
            text: string,
            render: (s: string) => string
        ): string
        {
            return `<b>${render(text)}</b>`;
        }
        function boldify(
        ): (text: string, render: (s: string) => string) => string
        {
            return boldifyImpl;
        }
        n2.on("input", checkResult);
        const msg: _Msg = {
            "boldify": boldify,
            "tableName": "foobar"
        };
        n1.receive(msg);
    }

    private _testSubstitution(done: (err?: Error) => void): void
    {
        const self = this;
        const n1 = self.getNode("n1");
        const n2 = self.getNode("n2");
        function checkResult(msg: NodeMessageInFlow): void
        {
            try
            {
                const payload = msg.payload as _Payload;
                const query = payload.command;
                const result = !query.includes("}")
                    && query.includes("test_table");
                result.should.be.true("Substitution does not work");
                done();
            }
            catch (error)
            {
                done(error as Error);
            }
        }
        n2.on("input", checkResult);
        const msg: _Msg = {
            "tableName": "test_table"
        };
        n1.receive(msg);
    }
}


const _TEST = new _ThisTest();
_TEST.run();
