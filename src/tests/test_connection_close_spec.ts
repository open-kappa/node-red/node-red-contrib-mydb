import "should";
import {
    MYDB_RUN_MODE,
    type MyDbNodeDef,
    MyDbPoolMock
} from "../node-red-contrib-mydb/src/mydbImpl";
import {
    MyNodeRedTest,
    type TestFlowsItem
} from "@open-kappa/mytest";
import MYDB_BUILDER
    from "../node-red-contrib-mydb/node-red-contrib-mydb";
import MYDB_CONFIG_BUILDER
    from "../node-red-contrib-mydb/node-red-contrib-mydbConfig";
import type {
    NodeMessage
} from "node-red";


MYDB_RUN_MODE.setTesting(true);


class _ThisTest
    extends MyNodeRedTest
{
    public constructor()
    {
        super("mydb connection close", [MYDB_BUILDER, MYDB_CONFIG_BUILDER]);
    }

    protected registerFlows(): void
    {
        const self = this;
        self.registerFlow(
            "Close connection on error",
            self._flowClose.bind(self)
        );
    }

    protected registerTests(): void
    {
        const self = this;
        self.registerTest(
            "Close connection on error",
            self._testClose.bind(self)
        );
    }

    private _flowClose(): Array<TestFlowsItem>
    {
        const self = this;

        const opts: Partial<MyDbNodeDef> = {
            "mydbConfig": "nodeCfg",
            // Passing an object: so it will fail!
            "query": {} as unknown as string
        };

        const cfgNode = self.makeNode("nodeCfg", "nodeCfg1", "mydbConfig");
        let ret = [cfgNode];

        /* eslint-disable function-call-argument-newline */
        ret = ret.concat(
            self.makeNodeWithSourceAndSink(
                "n0", "node0",
                "n1", "node1", "mydb",
                "n2", "node2",
                opts
            )
        );

        const catchNode = self.makeCatchNode(
            "n3",
            "n catch all",
            [["n4"]]
        );
        const catchSync = self.makeSinkNode("n4", "n catch sync");
        ret.push(catchSync);
        ret.push(catchNode);

        /* eslint-enable function-call-argument-newline */

        return ret;
    }

    private _testClose(done: (err?: Error) => void): void
    {
        const self = this;
        const n1 = self.getNode("n1");
        const n2 = self.getNode("n2");
        const nCatchSync = self.getNode("n4");
        function reportError(_msg: unknown): void
        {
            const error = new Error("It should have been failed");
            done(error);
        }
        function checkResult(_msg: unknown): void
        {
            const pool = MyDbPoolMock.lastInstance;
            if (pool === null)
            {
                done(new Error("Unable to find the pool"));
            }
            else if (pool.isConnected)
            {
                done(new Error("Disconnect not called"));
            }
            done();
        }
        n2.on("input", reportError);
        nCatchSync.on("input", checkResult);
        type _Msg = NodeMessage & {
            tableName: string;
        };
        const msg: _Msg = {
            "tableName": "test_table"
        };
        n1.receive(msg);
    }
}

const _TEST = new _ThisTest();
_TEST.run();
