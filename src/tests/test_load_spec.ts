import {
    MYDB_RUN_MODE,
    type MyDbNodeDef
} from "../node-red-contrib-mydb/src/mydbImpl";
import {
    MyNodeRedTest,
    type TestFlowsItem
} from "@open-kappa/mytest";
import MYDB_BUILDER
    from "../node-red-contrib-mydb/node-red-contrib-mydb";
import MYDB_CONFIG_BUILDER
    from "../node-red-contrib-mydb/node-red-contrib-mydbConfig";


MYDB_RUN_MODE.setTesting(true);


class _ThisTest extends MyNodeRedTest
{
    public constructor()
    {
        super("mydb load test", [MYDB_BUILDER, MYDB_CONFIG_BUILDER]);
    }

    protected registerFlows(): void
    {
        const self = this;
        self.registerFlow(
            "Should load",
            self._flowLoad.bind(self)
        );
    }

    protected registerTests(): void
    {
        const self = this;
        self.registerTest(
            "Should load",
            self._testLoad.bind(self)
        );
    }

    private _flowLoad(): Array<TestFlowsItem>
    {
        const self = this;
        const opts: Partial<MyDbNodeDef> = {"mydbConfig": "nodeCfg"};
        return [
            self.makeNode("nodeCfg", "nodeConfig1", "mydbConfig"),
            self.makeNode<MyDbNodeDef>("n1", "node1", "mydb", opts)
        ];
    }

    private _testLoad(): void
    {
        const self = this;
        const n1 = self.getNode("n1");
        n1.should.have.property("name", "node1");
    }
}

const _TEST = new _ThisTest();
_TEST.run();
