import type {PoolClient} from "pg";

export interface IDbConnection
{
    connect(): Promise<PoolClient>;
}
