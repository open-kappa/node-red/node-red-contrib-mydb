import {
    type IDbConnection,
    MYDB_RUN_MODE
} from "./mydbImpl";
import type {
    Node,
    NodeAPI,
    NodeDef
} from "node-red";
import type {
    PoolClient,
    PoolConfig
} from "pg";
import fs from "fs";
import {
    IMyNode
} from "@open-kappa/node-red-contrib-myutils";

export type MyDbConfigNodeDef = NodeDef & {
    host: string;
    port: number;
    database: string;
    ssl: boolean;
    rejectUnauthorized: boolean;
    ca: string;
    key: string;
    cert: string;
    user: string;
    password: string;
    idle: number;
    min: number;
    max: number;
};

export type MyDbConfigCreds = {
    user: string;
    password: string;
};

/**
 * The DB configuration.
 */
export class MyDbConfig
    extends IMyNode<MyDbConfigNodeDef, MyDbConfigCreds>
{
    private readonly _ca: string;
    private readonly _cert: string;
    private _connection: IDbConnection | null;
    private readonly _database: string;
    private readonly _host: string;
    private readonly _idle: number;
    private readonly _key: string;
    private readonly _max: number;
    private readonly _min: number;
    private readonly _password: string;
    private readonly _port: number;
    private readonly _rejectUnauthorized: boolean;
    private readonly _ssl: boolean;
    private readonly _user: string;

    /**
     * Constructor.
     * @param nodeRed - The nodeRed instance.
     * @param node - The reference node-red node.
     * @param config - The input configuration.
     */
    public constructor(
        nodeRed: NodeAPI,
        node: Node<MyDbConfigCreds>,
        config: MyDbConfigNodeDef
    )
    {
        super(nodeRed, node, config);

        this._ca = config.ca;
        this._cert = config.cert;
        this._connection = null;
        this._database = config.database;
        this._host = config.host;
        this._idle = Number(config.idle);
        this._key = config.key;
        this._max = Number(config.max);
        this._min = Number(config.min);
        this._password = node.credentials.password;
        this._port = Number(config.port);
        this._rejectUnauthorized = config.rejectUnauthorized;
        this._ssl = Boolean(config.ssl);
        this._user = node.credentials.user;
    }

    /**
     * Connects and returns a client on which perform query() and release().
     * @returns The pool client.
     */
    public async connect(): Promise<PoolClient>
    {
        this._node.debug("Connecting to DB...");
        const self = this;
        async function makeConnection(): Promise<IDbConnection>
        {
            if (self._connection === null)
            {
                return self._makeConnection();
            }
            return Promise.resolve(self._connection);
        }
        async function doConnect(conn: IDbConnection): Promise<PoolClient>
        {
            self._connection = conn;
            return self._connection.connect();
        }
        return makeConnection()
            .then(doConnect);
    }

    /**
     * Callback on node close events.
     * @param isRemoved - True if the node is removed.
     */
    protected async onCloseImpl(isRemoved: boolean): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        if (isRemoved)
        {
            // Do something
        }
        else
        {
            // Do something
        }
        return Promise.resolve();
    }

    /**
     * Creates a connection to the DB.
     * @returns The connection class instance.
     */
    private async _makeConnection(): Promise<IDbConnection>
    {
        const self = this;
        const config: PoolConfig = {
            "database": self._database,
            "host": self._host,
            "idleTimeoutMillis": self._idle,
            "max": self._max,
            "min": self._min,
            "password": self._password,
            "port": self._port,
            "ssl": self._ssl,
            "user": self._user
        };
        if (self._ssl)
        {
            config.ssl = {
                // eslint-disable-next-line no-undefined
                "ca": self._ca !== "" ? self._ca : undefined,
                // eslint-disable-next-line no-undefined
                "cert": self._cert !== "" ? self._cert : undefined,
                // eslint-disable-next-line no-undefined
                "key": self._key !== "" ? self._key : undefined,
                "rejectUnauthorized": self._rejectUnauthorized
            };
        }
        else
        {
            delete config.ssl;
        }
        async function readFile(
            conf: PoolConfig,
            field: "ca" | "cert" | "key"
        ): Promise<void>
        {
            if (typeof conf.ssl === "boolean"
                || typeof conf.ssl === "undefined"
                || typeof conf.ssl[field] !== "string"
                || conf.ssl[field] === ""
            )
            {
                return Promise.resolve();
            }
            const ssl = conf.ssl;
            async function doAssign(what: Buffer): Promise<void>
            {
                ssl[field] = what.toString();
                return Promise.resolve();
            }
            return fs.promises.readFile(ssl[field] as string)
                .then(doAssign);
        }
        async function readCaFile(): Promise<void>
        {
            return readFile(config, "ca");
        }
        async function readKeyFile(): Promise<void>
        {
            return readFile(config, "key");
        }
        async function readCertFile(): Promise<void>
        {
            return readFile(config, "cert");
        }
        async function doConnect(): Promise<IDbConnection>
        {
            return Promise.resolve(MYDB_RUN_MODE.makeConnection(config));
        }
        return readCaFile()
            .then(readKeyFile)
            .then(readCertFile)
            .then(doConnect);
    }
}
