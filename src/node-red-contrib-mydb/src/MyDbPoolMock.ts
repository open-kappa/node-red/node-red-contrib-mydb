import type {
    PoolClient,
    QueryResult
} from "pg";
import events from "events";
import type stream from "stream";

export type MockResult = QueryResult<Record<string, unknown>> & {
    // Undocumented on purpose:
    "isTesting": true;
};

/**
 * Mock for DB pool
 */
export class MyDbPoolMock
    extends events.EventEmitter
    implements PoolClient
{
    public static lastInstance: MyDbPoolMock | null = null;
    public isConnected: boolean;

    /**
     * Constructor.
     */
    public constructor()
    {
        super();
        this.isConnected = false;
        // eslint-disable-next-line @typescript-eslint/no-this-alias
        MyDbPoolMock.lastInstance = this;
    }

    /**
     * Mocks to connect to the DB.
     */
    public async connect(): Promise<void>
    {
        const self = this;
        self.isConnected = true;
        return Promise.resolve();
    }

    public copyFrom(_queryText: string): stream.Writable
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        const ret = null as unknown as stream.Writable;
        return ret;
    }

    public copyTo(_queryText: string): stream.Readable
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        const ret = null as unknown as stream.Readable;
        return ret;
    }

    public escapeIdentifier(str: string): string
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return str;
    }

    public escapeLiteral(str: string): string
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return str;
    }

    public on(_event: unknown): this
    {
        return this;
    }

    public pauseDrain(): void
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
    }

    /**
     * Mocks a query.
     * @param command - The SQL command to execute.
     * @returns A promise with the query result.
     */
    public async query(command: unknown): Promise<MockResult>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        if (typeof command !== "string")
        {
            throw new Error("command must be a string");
        }
        return Promise.resolve({
            "command": command,
            "fields": [],
            "isTesting": true,
            "oid": 123456,
            "rowCount": 0,
            "rows": []
        });
    }

    /**
     * Mocks the release of the client.
     * @returns A promise to wait the completion.
     */
    public async release(): Promise<void>
    {
        const self = this;
        self.isConnected = false;
        return Promise.resolve();
    }

    public resumeDrain(): void
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
    }
}
