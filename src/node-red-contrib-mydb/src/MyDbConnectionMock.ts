import {
    type IDbConnection,
    MyDbPoolMock
} from "./mydbImpl";
import type {
    PoolClient,
    PoolConfig
} from "pg";

/**
 * Class used to mock a connection to a DB during testing.
 */
export class MyDbConnectionMock
implements IDbConnection
{
    /**
     * Constructor.
     * @param _config - The connection configuration.
     */
    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    public constructor(_config: PoolConfig)
    {
        // Ntd
    }

    /**
     * Mocks to connect to the DB.
     * @returns The client instance.
     */
    public async connect(): Promise<PoolClient>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        const client = new MyDbPoolMock();
        client.isConnected = true;
        return Promise.resolve(client);
    }
}
