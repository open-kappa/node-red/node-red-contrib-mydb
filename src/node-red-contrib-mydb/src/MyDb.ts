import {
    getNode,
    getNodeModel,
    IMyNode,
    type OutputMessageType
} from "@open-kappa/node-red-contrib-myutils";
import {
    MyDbConfig,
    type MyDbConfigCreds
} from "./mydbImpl";
import type {
    Node,
    NodeAPI,
    NodeDef,
    NodeMessageInFlow
} from "node-red";
import type {
    PoolClient,
    QueryResult
} from "pg";
import handlebars from "handlebars";
import mustache from "mustache";


/** Allowable template styles */
export type Style = "handlebars" | "mustache";
/**
 * Default template style used if the configuration does not specify one.
 * For backwards-compatibility with previous versions, it is "mustache".
 */
const _DEFAULT_STYLE: Style = "mustache";

export type MyDbNodeDef = NodeDef & {
    "style"?: Style;
    "substEnvVars"?: boolean;
    "query"?: string;
    "mydbConfig"?: string;
};

/**
 * The actual DB worker.
 */
export class MyDb
    extends IMyNode<MyDbNodeDef>
{
    private readonly _myDbConfig: MyDbConfig | null;
    private readonly _query: string;
    private readonly _style: Style;
    private readonly _substEnvVars: boolean;

    /**
     * Constructor.
     * @param nodeRed - The nodeRed instance.
     * @param node - The reference node-red node.
     * @param config - The input configuration.
     */
    public constructor(nodeRed: NodeAPI, node: Node, config: MyDbNodeDef)
    {
        super(nodeRed, node, config);
        this._myDbConfig = null;
        this._query = config.query ?? "";
        this._style = config.style ?? _DEFAULT_STYLE;
        this._substEnvVars = config.substEnvVars ?? false;

        if (typeof config.mydbConfig !== "undefined")
        {
            const configNode = getNode<MyDbConfigCreds>(
                this._nodeRed,
                config.mydbConfig
            );
            if (configNode !== null && !(configNode instanceof Error))
            {
                const configNodeModel = getNodeModel<MyDbConfig>(configNode);
                if (!(configNodeModel instanceof MyDbConfig))
                {
                    throw new Error("BUG: wrong config node type!");
                }
                this._myDbConfig = configNodeModel;
            }
        }
    }

    protected override async onCloseImpl(removed: boolean): Promise<void>
    {
        const self = this;
        if (removed)
        {
            // Do something
        }
        else
        {
            // Do something
        }

        self._node.status({});
        return Promise.resolve();
    }

    protected override async onInputImpl(
        msg: NodeMessageInFlow
    ): Promise<OutputMessageType>
    {
        const self = this;
        const errorInfo = {
            "query": "",
            "result": {},
            "step": ""
        };
        self._node.debug("onInput() called");
        if (self._myDbConfig === null)
        {
            errorInfo.step = "initial";
            return Promise.reject(
                new Error("Missing configuration node for MyDB")
            );
        }
        const myDbConfig = self._myDbConfig;
        let client: PoolClient | null = null;
        const ret = msg;

        async function connect(): Promise<void>
        {
            errorInfo.step = "connect";
            async function doRet(poolClient: PoolClient): Promise<void>
            {
                client = poolClient;
                return Promise.resolve();
            }
            return myDbConfig.connect()
                .then(doRet);
        }

        async function substituteEnvVars(): Promise<string>
        {
            errorInfo.step = "substitute";
            errorInfo.query = self._query;
            const queryPromise = self._substituteEnvVars(self._query, msg);
            return queryPromise;
        }

        async function renderTemplate(inQuery: string): Promise<string>
        {
            errorInfo.step = "render";
            errorInfo.query = inQuery;
            const queryPromise = self._render(inQuery, msg);
            return Promise.resolve(queryPromise);
        }

        async function doQuery(query: string): Promise<QueryResult>
        {
            errorInfo.step = "query";
            errorInfo.query = query;
            self._node.debug(`Performed query: ${query}`);
            if (client === null)
            {
                throw new Error("Internal bug");
            }
            return client.query(query);
        }

        async function setResult(result: QueryResult): Promise<void>
        {
            errorInfo.step = "result";
            errorInfo.result = result;
            ret.payload = result;
            return Promise.resolve();
        }

        async function disconnect(): Promise<void>
        {
            if (client === null) return Promise.resolve();
            errorInfo.step = "disconnect";
            try
            {
                client.release();
            }
            catch (err)
            {
                client = null;
                return Promise.reject(err);
            }
            client = null;
            return Promise.resolve();
        }

        async function doCleanup(): Promise<OutputMessageType>
        {
            self._node.status({});
            return Promise.resolve(ret);
        }

        async function onError(error: Error): Promise<OutputMessageType>
        {
            self._node.status(
                {
                    "fill": "red",
                    "shape": "ring",
                    "text": "Error"
                }
            );
            async function doRet(): Promise<OutputMessageType>
            {
                return Promise.reject(error);
            }
            const msgAny = msg as unknown as Record<string, unknown>;
            msgAny.errorInfo = errorInfo;
            return disconnect()
                .then(doRet);
        }

        self._node.status(
            {
                "fill": "blue",
                "shape": "dot",
                "text": "Running"
            }
        );

        return connect()
            .then(substituteEnvVars)
            .then(renderTemplate)
            .then(doQuery)
            .then(setResult)
            .then(disconnect)
            .then(doCleanup)
            .catch(onError);
    }

    /**
     * Render a query using the given data and the currently
     * configured template style.
     * @param inQuery - The query string.
     * @param msg - The message.
     * @returns The result.
     */
    private async _render(
        inQuery: string,
        msg: NodeMessageInFlow
    ): Promise<string>
    {
        const self = this;
        const view = {
            "msg": msg
        };
        self._node.debug(
            `Rendering ${self._style} query '${inQuery}'`
            // + ` with ${JSON.stringify(view)}`
        );

        function mustacheEngine(qry: string, vw: typeof view): string
        {
            return mustache.render(qry, vw);
        }
        function handlebarsEngine(qry: string, vw: typeof view): string
        {
            const template = handlebars.compile(qry);
            return template(vw);
        }
        const engines = {
            "handlebars": handlebarsEngine,
            "mustache": mustacheEngine
        };
        const engine = engines[self._style];
        if (typeof engine === "undefined")
        {
            throw new Error(`Unknown template style ${self._style}`);
        }

        const query = engine(inQuery, view);
        return Promise.resolve(query);
    }

    /**
     * Substitute enviroment variables.
     * @param query - The input query.
     * @param msg - The input message.
     * @returns The resulting query.
     */
    private async _substituteEnvVars(
        query: string,
        msg: NodeMessageInFlow
    ): Promise<string>
    {
        const self = this;
        if (!self._substEnvVars) return Promise.resolve(query);
        self._node.debug(`Query before env-evaluation: ${query}`);

        const newQuery = self._nodeRed.util.evaluateNodeProperty(
            query,
            "env",
            self._node,
            msg
        ) as string;
        return Promise.resolve(newQuery);
    }
}
