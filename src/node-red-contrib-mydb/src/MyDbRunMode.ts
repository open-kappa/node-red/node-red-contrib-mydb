import {
    type IDbConnection,
    MyDbConnectionMock
} from "./mydbImpl";
import pg, {
    type PoolClient,
    type PoolConfig
} from "pg";

/**
 * Singleton for global running configuration.
 * This module exports the singleton instance directly.
 */
export class MyDbRunMode
{
    private _isTesting: boolean;

    /**
     * Constructor.
     */
    public constructor()
    {
        this._isTesting = false;
    }

    /**
     * Gets the isTesting flag.
     * @returns True if it is testing.
     */
    public isTesting(): boolean
    {
        return this._isTesting;
    }

    /**
     * Returns the connection pool.
     * If isTesting holds, returns an instance of MyDbConnectionMock, otherwise
     * returns the actual DB connection class instance.
     * @param config - The connection config.
     * @returns The connection instance.
     */
    public makeConnection(config: PoolConfig): IDbConnection
    {
        if (this._isTesting) return new MyDbConnectionMock(config);
        const ret = new pg.Pool(config);
        function onError(err: Error, _client: PoolClient): void
        {
            // eslint-disable-next-line no-console
            console.error(`Error inside pg: ${String(err)}`);
        }
        ret.on("error", onError);
        return ret;
    }

    /**
     * Sets whether it is running in regression tests.
     * Therefore, it sets whether to use mock classes.
     * @param isTesting - True if it is testing.
     */
    public setTesting(isTesting: boolean): void
    {
        this._isTesting = isTesting;
    }
}

export const MYDB_RUN_MODE = new MyDbRunMode();
