import {
    getMyBuilder,
    type TMyCustomNodeClassDef
} from "@open-kappa/node-red-contrib-myutils";
import {
    MyDbConfig,
    type MyDbConfigCreds
} from "./src/mydbImpl";

const _MYDB_CONFIG: TMyCustomNodeClassDef<MyDbConfig, MyDbConfigCreds> = {
    "name": "mydbConfig",
    "opts": {
        "credentials": {
            "password": {
                "type": "password"
            },
            "user": {
                "type": "text"
            }
        }
    },
    "UserClass": MyDbConfig
};

const MYDB_CONFIG_BUILDER = getMyBuilder(_MYDB_CONFIG);

module.exports = MYDB_CONFIG_BUILDER;
export default MYDB_CONFIG_BUILDER;
