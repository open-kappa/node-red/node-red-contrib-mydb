import {
    getMyBuilder,
    type TMyCustomNodeClassDef
} from "@open-kappa/node-red-contrib-myutils";
import {
    MyDb
} from "./src/mydbImpl";


const _MYDB_NODE: TMyCustomNodeClassDef<MyDb> = {
    "name": "mydb",
    "UserClass": MyDb
};

const MYDB_BUILDER = getMyBuilder(_MYDB_NODE);

module.exports = MYDB_BUILDER;
export default MYDB_BUILDER;
