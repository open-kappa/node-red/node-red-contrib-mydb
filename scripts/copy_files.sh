#!/bin/bash

function get_script_dir()
{
    local DIR=""
    local SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
        DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
        SOURCE="$(readlink "$SOURCE")"
        [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    done
    THIS_SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
}
get_script_dir

function build_html()
{
    local NODE_NAME="$1"
    if [ -f src/$NODE_NAME/$NODE_NAME.html ]; then
        cp -r src/$NODE_NAME/*.html dist/src/$NODE_NAME
        if [ "$?" != "0" ]; then
            exit 1
        fi
    fi
}

function build_icon()
{
    local NODE_NAME="$1"
    if [ -d src/$NODE_NAME/icons ]; then
        cp -r src/$NODE_NAME/icons dist/src/$NODE_NAME
        if [ "$?" != "0" ]; then
            exit 1
        fi
    fi
}

function build_json()
{
    local NODE_NAME="$1"
    if [ -d src/$NODE_NAME/json ]; then
        cp -r src/$NODE_NAME/json dist/src/$NODE_NAME
        if [ "$?" != "0" ]; then
            exit 1
        fi
    fi
}

function build_node()
{
    local NODE_NAME="$1"
    mkdir -p dist/src/$NODE_NAME
    build_html $NODE_NAME
    build_icon $NODE_NAME
    build_json $NODE_NAME
}

function build_ts()
{
    if [ -f "node_modules/typescript/bin/tsc" ]; then
        node_modules/typescript/bin/tsc
    elif [ -f "node_modules/.bin/tsc" ]; then
        node_modules/.bin/tsc
    else
        tsc
    fi
    if [ "$?" != "0" ]; then
        exit 1
    fi
}

cd $THIS_SCRIPT_DIR/..
# build_ts
build_node node-red-contrib-mydb
build_node tests

# EOF
