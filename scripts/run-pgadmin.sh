#!/bin/bash

mkdir -p /tmp/test-pgadmin
mkdir -p /tmp/test-pgadmin/var/lib
chmod -R a+rw /tmp/test-pgadmin/var

docker run --rm \
    -v /tmp/test-pgadmin/var/lib:/var/lib/pgadmin \
    -e PGADMIN_DEFAULT_EMAIL=admin@admin.it \
    -e PGADMIN_DEFAULT_PASSWORD=admin \
    -p 8080:80 \
    --name test-pgadmin dpage/pgadmin4:latest

# EOF
