#!/bin/bash

mkdir -p /tmp/test-postgres
mkdir -p /tmp/test-postgres/data
chmod -R a+rw /tmp/test-postgres/data


docker run --rm \
    -v /tmp/test-postgres/data:/var/lib/postgresql/data \
    -e POSTGRES_USER=postgres \
    -e POSTGRES_PASSWORD=postgres \
    -p 5432:5432 \
    --name test-postgres postgres:latest

# EOF
