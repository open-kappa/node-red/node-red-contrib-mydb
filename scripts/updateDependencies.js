const fs = require('fs');
const child_process = require('child_process');

function readPackage()
{
    const file = fs.readFileSync('package.json');
    return JSON.parse(file);
}

function _forAllDeps(foo, content)
{
    console.log("---- Peer");
    for (const dep in content.peerDependencies)
    {
        if (content.peerDependencies[dep].startsWith(".")
            || content.peerDependencies[dep].startsWith("/"))
        {
            continue;
        }
        foo(content.peerDependencies, dep, "--save-peer");
    }
    console.log("---- Dev");
    for (const dep in content.devDependencies)
    {
        if (content.devDependencies[dep].startsWith(".")
        || content.devDependencies[dep].startsWith("/"))
        {
            continue;
        }
        foo(content.devDependencies, dep, "--save-dev");
    }
    console.log("---- Prod");
    for (const dep in content.dependencies)
    {
        if (content.dependencies[dep].startsWith(".")
            || content.dependencies[dep].startsWith("/"))
        {
            continue;
        }
        foo(content.dependencies, dep, "");
    }
    return content;
}


function _wipeDependencies()
{
    console.log("-- Wipe");
    function doWipe(dependencies, dep, _kind)
    {
        dependencies[dep] = '*';
        //console.log(`wiped: ${dep}`);
    }
    const content = _forAllDeps(doWipe, readPackage());
    fs.writeFileSync('package.json', JSON.stringify(content, null, 4) + "\n");
}

function _updateDependencies()
{
    console.log("-- Update");
    function doUpdate(_dependencies, dep, kind)
    {
        child_process.execSync(`npm update ${dep} ${kind}`);
        console.log(`updated: ${dep}`);
    }
    _forAllDeps(doUpdate, readPackage());
}

function _savePackages()
{
    console.log("-- Save");
    function doSave(_dependencies, dep, kind)
    {
        child_process.execSync(`npm install ${dep} ${kind}`);
        console.log(`installed: ${dep}`);
    }
    _forAllDeps(doSave, readPackage());
}

function _fixPackage(oldPackage)
{
    console.log("-- Fixing");
    const newPackage = readPackage();

    function doSub(oldDeps, newDeps, dep, what)
    {
        const oldValue = oldDeps[dep];
        if (oldValue.startsWith("~"))
        {
            newDeps[dep] = "~" + newDeps[dep].substring(1);
            return true;
        }
        else if (oldValue.startsWith(">="))
        {
            newDeps[dep] = oldValue;
            return true;
        }
        else if (oldValue.startsWith(">"))
        {
            newDeps[dep] = oldValue;
            return true;
        }
        else if (oldValue.match(/^[0-9]/u))
        {
            // Case of exact version:
            newDeps[dep] = newDeps[dep].substring(1);
            return true;
        }
        return false;
    }
    function replace(oldDeps, dep, kind)
    {
        switch (kind)
        {
            case "--save-peer":
                return doSub(oldDeps, newPackage.peerDependencies, dep);
            case "--save-dev":
                return doSub(oldDeps, newPackage.devDependencies, dep);
            case "":
                return doSub(oldDeps, newPackage.dependencies, dep);
            default:
                console.error(`Unexpected kind: ${kind}`)
                process.exit(1);
        }
    }
    function doFix(dependencies, dep, kind)
    {
        const done = replace(dependencies, dep, kind);
        if (!done) return;
        console.log(`fixed: ${dep}`);
    }

    _forAllDeps(doFix, oldPackage);
    fs.writeFileSync('package.json', JSON.stringify(newPackage, null, 4) + "\n");
}

if (require.main === module)
{
    const oldPackage = readPackage();
    _wipeDependencies();
    _updateDependencies();
    _savePackages();
    _fixPackage(oldPackage);
}
else
{
    module.exports = {
        "forAllDeps": _forAllDeps,
        "wipeDependencies": _wipeDependencies,
        "updateDependencies": _updateDependencies,
        "savePackages": _savePackages,
        "fixPackage": _fixPackage
    }
}
